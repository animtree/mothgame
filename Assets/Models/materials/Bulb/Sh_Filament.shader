// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "New AmplifyShader"
{
	Properties
	{
		_Color1("Color 1", Color) = (0.9184584,0.9758621,0,0)
		_AOffset("A Offset", Float) = 0
		_ALength("A Length", Float) = 0
		_flicker_smooth("flicker_smooth", 2D) = "white" {}
		_Float0("Float 0", Float) = 0
		_Float1("Float 1", Float) = 0.3
		_Float2("Float 2", Float) = 0.7
		_Float3("Float 3", Float) = 0
		_ClampBoost("ClampBoost", Float) = 0
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry+0" "IsEmissive" = "true"  }
		Cull Back
		CGPROGRAM
		#include "UnityShaderVariables.cginc"
		#pragma target 3.0
		#pragma surface surf Standard keepalpha addshadow fullforwardshadows vertex:vertexDataFunc 
		struct Input
		{
			float eyeDepth;
		};

		uniform float4 _Color1;
		uniform float _ALength;
		uniform float _AOffset;
		uniform sampler2D _flicker_smooth;
		uniform float _Float0;
		uniform float _Float1;
		uniform float _ClampBoost;
		uniform float _Float2;
		uniform float _Float3;

		void vertexDataFunc( inout appdata_full v, out Input o )
		{
			UNITY_INITIALIZE_OUTPUT( Input, o );
			o.eyeDepth = -UnityObjectToViewPos( v.vertex.xyz ).z;
		}

		void surf( Input i , inout SurfaceOutputStandard o )
		{
			o.Albedo = float4(0,0,0,0).rgb;
			float cameraDepthFade18 = (( i.eyeDepth -_ProjectionParams.y - _AOffset ) / _ALength);
			float4 temp_output_24_0 = ( _Color1 + cameraDepthFade18 );
			float clampResult28 = clamp( _SinTime.x , 0.3 , 0.7 );
			float4 temp_output_26_0 = ( temp_output_24_0 + clampResult28 );
			float2 temp_cast_1 = (_Float0).xx;
			float2 panner29 = ( float2( 0,0 ) + 1.0 * _Time.y * temp_cast_1);
			float4 tex2DNode31 = tex2D( _flicker_smooth, panner29 );
			float temp_output_47_0 = ( cameraDepthFade18 * _ClampBoost );
			float4 temp_cast_2 = (( _Float1 + temp_output_47_0 )).xxxx;
			float4 temp_cast_3 = (( _Float2 + temp_output_47_0 )).xxxx;
			float4 clampResult35 = clamp( tex2DNode31 , temp_cast_2 , temp_cast_3 );
			o.Emission = ( ( temp_output_26_0 * clampResult35 ) + _Float3 ).rgb;
			o.Alpha = 1;
		}

		ENDCG
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=14401
162;697;1906;1004;795.1737;339.951;1.3;True;True
Node;AmplifyShaderEditor.RangedFloatNode;17;-818.0728,380.2732;Float;False;Property;_ALength;A Length;7;0;Create;True;0;1.11;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;16;-812.5702,471.9839;Float;False;Property;_AOffset;A Offset;5;0;Create;True;0;-1.4;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;44;-64.1733,1230.021;Float;False;Property;_ClampBoost;ClampBoost;14;0;Create;True;0;0.03;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;34;-397.4167,1110.978;Float;False;Property;_Float0;Float 0;10;0;Create;True;0;0.5;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.CameraDepthFade;18;-631.384,366.9506;Float;False;3;2;FLOAT3;0,0,0;False;0;FLOAT;1.0;False;1;FLOAT;0.0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;37;438.9543,1095.86;Float;False;Property;_Float2;Float 2;12;0;Create;True;0.7;0.51;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.PannerNode;29;-228.3612,943.5591;Float;True;3;0;FLOAT2;0,0;False;2;FLOAT2;10,0;False;1;FLOAT;1.0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RangedFloatNode;36;448.1207,980.9302;Float;False;Property;_Float1;Float 1;11;0;Create;True;0.3;0.5;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SinTimeNode;25;-646.4562,669.6902;Float;True;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;23;-685.1807,185.9149;Float;False;Property;_Color1;Color 1;1;0;Create;True;0.9184584,0.9758621,0,0;1,0.8392157,0.3529412,1;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;47;154.2918,1010.029;Float;True;2;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ClampOpNode;28;-398.5369,699.0944;Float;False;3;0;FLOAT;0.0;False;1;FLOAT;0.3;False;2;FLOAT;0.7;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;46;640.1082,1104.748;Float;False;2;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;31;92.0501,773.9207;Float;True;Property;_flicker_smooth;flicker_smooth;9;0;Create;True;761f29ee7809777499d07b01434bbb3a;761f29ee7809777499d07b01434bbb3a;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0.0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1.0;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleAddOpNode;45;643.1638,982.53;Float;False;2;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;24;-338.4754,317.01;Float;True;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleAddOpNode;26;-3.236545,509.9796;Float;True;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0.0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.ClampOpNode;35;880.4849,920.5599;Float;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;39;182.9306,-77.83929;Float;True;Property;_Float3;Float 3;13;0;Create;True;0;0.5;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;33;599.9318,539.8114;Float;True;2;2;0;COLOR;0.0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;15;-1202.275,205.7942;Float;False;Property;_EOffset;E Offset;6;0;Create;True;0;0.34;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;32;391.4864,434.7023;Float;True;2;2;0;COLOR;0.0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.ColorNode;4;-537.524,-503.3714;Float;False;Property;_AlbedoA;Albedo A;3;0;Create;True;0.9184584,0.9758621,0,0;0.9184584,0.9758621,0,0;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;8;-534.0688,-331.2056;Float;False;Property;_AlbedoB;Albedo B;4;0;Create;True;0.9184584,0.9758621,0,0;0.9184584,0.9758621,0,0;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;27;-232.5368,674.0944;Float;True;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.ColorNode;11;-1132.808,-184.5427;Float;False;Property;_EmmissiveB;Emmissive B;2;0;Create;True;0.9184584,0.9758621,0,0;0.8235294,1,0.8758621,1;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.LerpOp;40;-648.7625,-89.93398;Float;True;3;0;COLOR;0.0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0.0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.LerpOp;7;-217.2186,-386.2424;Float;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.ColorNode;10;-1135.956,-344.4867;Float;False;Property;_EmmissiveA;Emmissive A;0;0;Create;True;0.9184584,0.9758621,0,0;1,0.8392158,0.3529412,1;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.CameraDepthFade;2;-1027.199,112.9827;Float;False;3;2;FLOAT3;0,0,0;False;0;FLOAT;1.0;False;1;FLOAT;0.0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;48;127.9695,-215.553;Float;False;Constant;_Color2;Color 2;15;0;Create;True;0.09558821,0.09558821,0.09558821,0;0,0,0,0;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;20;-262.4398,-175.0433;Float;False;Constant;_Color0;Color 0;8;0;Create;True;0,0,0,0;0,0,0,0;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleAddOpNode;41;-181.8825,420.4337;Float;True;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;14;-1201.666,130.8885;Float;False;Property;_ELength;E Length;8;0;Create;True;0;2;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;42;-50.42288,128.5309;Float;True;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0.0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleAddOpNode;38;493.5188,-147.1057;Float;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0.0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;939.4511,-95.04176;Float;False;True;2;Float;ASEMaterialInspector;0;0;Standard;New AmplifyShader;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Back;0;0;False;0;0;False;0;Opaque;0.5;True;True;0;False;Opaque;;Geometry;All;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;False;0;255;255;0;0;0;0;0;0;0;0;False;2;15;10;25;False;0.5;True;0;Zero;Zero;0;Zero;Zero;OFF;OFF;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;0;0;0;False;0;0;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0.0;False;4;FLOAT;0.0;False;5;FLOAT;0.0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0.0;False;9;FLOAT;0.0;False;10;FLOAT;0.0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;18;0;17;0
WireConnection;18;1;16;0
WireConnection;29;2;34;0
WireConnection;47;0;18;0
WireConnection;47;1;44;0
WireConnection;28;0;25;1
WireConnection;46;0;37;0
WireConnection;46;1;47;0
WireConnection;31;1;29;0
WireConnection;45;0;36;0
WireConnection;45;1;47;0
WireConnection;24;0;23;0
WireConnection;24;1;18;0
WireConnection;26;0;24;0
WireConnection;26;1;28;0
WireConnection;35;0;31;0
WireConnection;35;1;45;0
WireConnection;35;2;46;0
WireConnection;33;0;26;0
WireConnection;33;1;35;0
WireConnection;32;0;26;0
WireConnection;32;1;31;0
WireConnection;27;0;24;0
WireConnection;27;1;28;0
WireConnection;40;0;10;0
WireConnection;40;1;11;0
WireConnection;40;2;2;0
WireConnection;7;0;4;0
WireConnection;7;1;8;0
WireConnection;7;2;18;0
WireConnection;2;0;14;0
WireConnection;2;1;15;0
WireConnection;38;0;33;0
WireConnection;38;1;39;0
WireConnection;0;0;20;0
WireConnection;0;2;38;0
ASEEND*/
//CHKSM=8474EF586C516F9F3113688F8EF141D77D7376A2