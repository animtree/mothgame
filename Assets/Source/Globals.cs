﻿using System.Collections;
using System.Collections.Generic;
using System.Net.Sockets;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Globals : MonoSingleton<Globals>
{
    IGameState m_state;
    public List<string> m_sceneNames;
    int m_currentSceneIdx= 0;

    public Player m_playerPrefab;
    
    void Start()
    {
        m_currentSceneIdx = m_sceneNames.IndexOf(SceneManager.GetActiveScene().name);
        DontDestroyOnLoad(gameObject);
        transform.SetAsLastSibling();
        
        if(IntroGameState.Me != null)
            IntroGameState.Me.EnterState();
    }
    
    public void OnStateChange(IGameState _newState)
    {
        m_state = _newState;
    }

    private bool isLoadingNextScene = false;
    public void LoadNextScene()
    {
        if (isLoadingNextScene)
            return;
        
        isLoadingNextScene = true;
        TransitionGameState.Me.OnComplete = () =>
        {
            isLoadingNextScene = false;
            if(MainGameState.Me != null)
                MainGameState.Me.EnterState();
            else if (IntroGameState.Me != null)
                IntroGameState.Me.EnterState();
        };
        TransitionGameState.Me.OnFaded = () => {
            m_currentSceneIdx++;
            m_currentSceneIdx %= m_sceneNames.Count;
            SceneManager.LoadScene(m_sceneNames[m_currentSceneIdx]);
        };
        TransitionGameState.Me.EnterState();
    }

    public void ReloadScene()
    {
        TransitionGameState.Me.OnFaded = () => {
            SceneManager.LoadScene(m_sceneNames[m_currentSceneIdx]);
        };
        TransitionGameState.Me.EnterState();
    }
}
