﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainGameState : GameState<MainGameState>, IGameState
{
	public override EGameState Type{get { return EGameState.main; }}

	StartBlock m_startBlock;
	
	public override void EnterState()
	{
		base.EnterState();
		
		PlacePlayer();
	}

	void PlacePlayer()
	{
		if(Player.Me == null)
			Instantiate(Globals.Me.m_playerPrefab, Globals.Me.transform);

		Player.Me.OnLevelLoad();
	}
	
	public void SetStartBlock(StartBlock _startblock)
	{
		m_startBlock = _startblock;
	}

	public override void ExitState()
	{
		base.ExitState();
	}
}
