﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class IntroGameState : GameState<IntroGameState>, IGameState
{
	public Button m_startGame;
	
	public override EGameState Type
	{
		get { return EGameState.intro; }
	}

	void Start()
	{
		m_startGame.onClick.AddListener(ExitState);
	}
	
	public override void EnterState()
	{
		base.EnterState();
	}

	public override void ExitState()
	{
		base.ExitState();
		Globals.Me.LoadNextScene();
	}
}
