﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class mothExit : PlayerInteractable
{
    public static mothExit Me;

    void Awake()
    {
        Me = this;
    }
    
    protected override void OnPlayerHit(mothmovement _moth)
    {
        _moth.on_exit();
    }
}
