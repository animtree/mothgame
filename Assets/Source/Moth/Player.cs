﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.Analytics;

public class Player : MonoSingleton<Player>
{
    public const string PLAYER_TAG = "Player";
    public bool has_won = false;

    private List<GameObject> m_currentMoths = new List<GameObject>();


    void Start()
    {
        OnLevelLoad();
    }
    
    public void OnLevelLoad()
    {
        has_won = false;
        for (int i = m_currentMoths.Count - 1; i >= 0; i--)
            Destroy(m_currentMoths[i].gameObject);
        m_currentMoths.Clear();

        var mothsInScene = GameObject.FindGameObjectsWithTag("Moth");
        m_currentMoths = mothsInScene.ToList();
    }

    public void CheckEndState()
    {
        GameObject[] moths = GameObject.FindGameObjectsWithTag("Moth");
        Debug.Log(moths.Length);
        if (moths.Length > 0)
            return;
        if (has_won)
        {
            StartCoroutine(StartNext());
        }
        else 
        {    
            StartCoroutine(StartRestart());  
        }
    }

    IEnumerator StartRestart()
    {
        yield return new WaitForSeconds(0.4f);
        Globals.Me.ReloadScene();
    }
    IEnumerator StartNext()
    {
        yield return new WaitForSeconds(1.5f);
        Globals.Me.LoadNextScene();
    }
}
