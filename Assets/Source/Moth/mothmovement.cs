﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class mothmovement : MonoBehaviour {
    public float light_speed = 10;
    public float light_distance = 20;
    public float avoidance_distance = 1;
    public float avoidance_speed = 6;
    public float attaction_distance = 8;
    public float attaction_speed = 4;
    public float attaction_speed_noise = 6;
    public float speed_noise = 1;
    public float noise_frequency = 60;
    public Canvas icon_canvas;
    public Sprite see_light_sprite;
    public Sprite no_light_sprite;
    public Image icon_image;

    public AudioSource audio_source;
    public AudioClip[] alert_clip;
    public AudioClip[] unalert_clip;
    public AudioClip[] victory_clip;

    public float m_canvasWorldOffset = 0.5f;
    public GameObject visual;

    public MeshRenderer m_meshRenderer;
    public Material m_seeLightMat;
    public Material m_noLightMat;
    
    private const float MAX_MAG = 2;

    public bool debug_lights;
    public bool debug_moths;

    private Rigidbody rb;
    private FastNoise fn;
    private float t = 0;
    private float scale;

    private int light_count = 0; //Did the moth see a light in the last update
    private int last_light_count = 0; //Same as above, but cannot update more than once per second
    private bool exit_animation = false;

    // Use this for initialization
    void Start () {
        rb = GetComponent<Rigidbody>();
        fn = new FastNoise((int)(Random.value * 10000));
        scale = gameObject.transform.localScale[0];
    }
	
	// Update is called once per frame
	void FixedUpdate () {
        if (fn == null)
        {
            return;
        }

        t += Time.deltaTime;

        //Update icon fade
        var c = icon_image.color;
        c.a -= (float)(10 * Time.deltaTime);
        icon_image.color = c;

        LayerMask mask = LayerMask.GetMask("Obstacle");
	    var lights = MothLight.s_allMothLights;
	    
        int count = 0;
        foreach (var light in lights)
        {
            if(!light.m_isLit)
                continue;
            
            Vector3 delta = light.transform.position - rb.position;
            if (delta.magnitude > light_distance * light.m_lightDistMod)
            {
                continue;
            }
            Vector3 heading = delta.normalized;


            RaycastHit hit;
            // Does the ray intersect any objects excluding the player layer
            if (Physics.Raycast(transform.position, heading, out hit, delta.magnitude, mask))
            {
                if(debug_lights)
                    Debug.DrawRay(transform.position, heading * hit.distance, Color.red);
            }
            else
            {
                if (debug_lights)
                    Debug.DrawRay(transform.position, delta, Color.yellow);
                float attraction = Mathf.Min(MAX_MAG, (1 / delta.magnitude / delta.magnitude) * light.m_lightStrength);
                count++;
                rb.AddForce(heading * attraction * light_speed * light_speed);
            }
        }

	   
	    m_meshRenderer.material = count > 0 ? m_seeLightMat : m_noLightMat;
	    
	    
        light_count = count;
	    if (count == 0)
	    {
	        var toExit = (mothExit.Me.transform.position - transform.position);
	        var sqrDist = toExit.sqrMagnitude;
	        float forceMag = MAX_MAG * 0.5f;
	        rb.AddForce(toExit.normalized * forceMag);
	    }

        if(light_count != last_light_count)
        {
            on_see_light(light_count > last_light_count);
            last_light_count = light_count;
        }

        GameObject[] moths = GameObject.FindGameObjectsWithTag("Moth");
        foreach (var moth in moths)
        {
            if(gameObject == moth)
            {
                continue;
            }

            Rigidbody moth_rb = moth.GetComponent<Rigidbody>();
            Vector3 delta = moth_rb.position - rb.position;
            Vector3 heading = delta.normalized;
            RaycastHit hit;
            // Does the ray intersect any objects excluding the player layer
            if (Physics.Raycast(transform.position, heading, out hit, delta.magnitude, mask))
            {
                continue;
            }

            if (delta.magnitude < attaction_distance)
            {
                if (delta.magnitude > avoidance_distance)
                {
                    if (debug_moths)
                        Debug.DrawRay(rb.position, delta, Color.green);

                    float n = fn.GetSimplex(t*noise_frequency, 0) * attaction_speed_noise;
                    rb.AddForce(heading * (attaction_speed + n));
                }else
                {
                    if (debug_moths)
                        Debug.DrawRay(rb.position, delta, Color.red);
                    float attraction = Mathf.Min(MAX_MAG, 1 / delta.magnitude / delta.magnitude);
                    rb.AddForce(heading * -1 * avoidance_speed * attraction);
                }
            }
        }

        float x = fn.GetSimplex(0, 0, t * noise_frequency) * speed_noise;
        float y = fn.GetSimplex(0, t * noise_frequency, 0) * speed_noise;
        rb.AddForce(new Vector3(x,y,0));
    }

    // Update is called once per frame
    void LateUpdate ()
    {    
        var dir = (rb.velocity);
        visual.transform.up = dir;

        if (exit_animation)
        {
            scale -= (float) (3 * Time.deltaTime);
            gameObject.transform.localScale = new Vector3(scale, scale, scale);
            if(scale < 0)
            {
                Destroy(gameObject);
                return;
            }
        }
        
        icon_canvas.transform.position = transform.position + Vector3.up * m_canvasWorldOffset;
        icon_canvas.transform.rotation = Quaternion.identity;
    }

    void on_see_light(bool see_light)
    {
        icon_image.sprite = see_light_sprite;
        icon_image.color = new Color(1, 1, 1, 5);
        if (see_light)
        {
            icon_image.sprite = see_light_sprite;
            audio_source.clip = alert_clip[Random.Range(0, alert_clip.Length)];
        }else
        {
            icon_image.sprite = no_light_sprite;
            audio_source.clip = unalert_clip[Random.Range(0, unalert_clip.Length)];
        }
        audio_source.pitch = (float) (Random.value * 0.2 + 0.9);
        audio_source.Play();
    }

    public void on_exit()
    {
        if (!exit_animation)
        {
            exit_animation = true;
            audio_source.clip = victory_clip[Random.Range(0, victory_clip.Length)];
            audio_source.pitch = (float)(Random.value * 0.2 + 0.9);
            audio_source.Play();
            Player.Me.has_won = true;
        }
    }

    void OnDestroy()
    {
        if(Player.Me != null)
            Player.Me.CheckEndState();
    }

}
