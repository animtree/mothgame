﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class MothLightObstacle : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
	public const int OBSTACLE_LAYER = 11;
	public const int OBSTACLE_DRAG_LAYER = 13;
	public const string ObstacleTag = "Obstacle";
	private static MothLightObstacle s_dragTarget;
	public static List<MothLightObstacle> s_allLightObstacles;

	public Collider m_collider;
	public Rigidbody m_rb;
	public bool m_isDraggable = true;
	public float m_rotationPerSeconds = 30f;

	public MeshRenderer m_meshrenderer;
	public Material m_dragMaterial;
	Material m_normalMaterial;
	
	private Vector3 m_dragStartOffset;
	private float m_dragScale = 1.05f;
	
	void Start()
	{
		if (s_allLightObstacles == null)
			s_allLightObstacles = new List<MothLightObstacle>();
		
		var children = gameObject.GetComponentsInChildren<Transform>();
		foreach (var child in children)
			child.gameObject.layer = OBSTACLE_LAYER;
		
		s_allLightObstacles.Add(this);
		gameObject.tag = ObstacleTag;
		m_normalMaterial = m_meshrenderer.material;
	}

	void OnDestroy()
	{
		s_allLightObstacles.Remove(this);
	}

	void StartDrag()
	{
		s_dragTarget = this;
		m_rb.Sleep();
		
		m_meshrenderer.material = m_dragMaterial;
		
//		var children = gameObject.GetComponentsInChildren<Transform>();
//		foreach (var child in children)
//			child.gameObject.layer = OBSTACLE_DRAG_LAYER;
		
		var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
		var m = - ray.origin.z / ray.direction.z;
		var pos = ray.origin + m * ray.direction;
		var offset = transform.position - pos;
		offset.z = 0f;
		m_dragStartOffset = offset;
	}

	void EndDrag()
	{
		s_dragTarget = null;
		m_rb.WakeUp();

//		var children = gameObject.GetComponentsInChildren<Transform>();
//		foreach (var child in children)
//			child.gameObject.layer = OBSTACLE_LAYER;
		
		m_meshrenderer.material = m_normalMaterial;
	}
	
	public void OnPointerDown(PointerEventData eventData)
	{
		if (!m_isDraggable || eventData.button != PointerEventData.InputButton.Left)
			return;
		StartDrag();
	}
	
	public void OnPointerUp(PointerEventData eventData)
	{
		if (s_dragTarget == null || eventData.button != PointerEventData.InputButton.Left)
			return;
		s_dragTarget.EndDrag();
	}

	void UpdateDrag()
	{
		var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
		var m = - ray.origin.z / ray.direction.z;
		var pos = ray.origin + m * ray.direction;
		pos += m_dragStartOffset;
		transform.position = pos;
	}

	void UpdateInput()
	{
		if(Input.GetKey(KeyCode.A))
			transform.RotateAroundLocal(Vector3.back, m_rotationPerSeconds * Time.deltaTime);
		if(Input.GetKey(KeyCode.D))
			transform.RotateAroundLocal(Vector3.back, -m_rotationPerSeconds * Time.deltaTime);
			
	}
	
	void Update()
	{
		if (this == s_dragTarget)
		{
			UpdateDrag();
			UpdateInput();
		}
	}
}
