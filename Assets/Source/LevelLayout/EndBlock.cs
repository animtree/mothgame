﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndBlock : PlayerInteractable
{
    protected override void OnPlayerHit(mothmovement _moth)
    {
        base.OnPlayerHit(_moth);
        
        Globals.Me.LoadNextScene();
    }
}
