﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartBlock : MonoBehaviour
{
    void Awake()
    {
        MainGameState.Me.SetStartBlock(this);
    }
}
