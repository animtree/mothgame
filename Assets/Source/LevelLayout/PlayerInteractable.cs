﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class PlayerInteractable : MonoBehaviour
{


    protected virtual void OnPlayerHit(mothmovement _moth){}
    
    void OnCollisionEnter(Collision collision)
    {
        if(collision.collider.gameObject.CompareTag("Moth"))
            OnPlayerHit(collision.collider.gameObject.GetComponent<mothmovement>());
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Moth"))
            OnPlayerHit(other.gameObject.GetComponent<mothmovement>());
    }
}
