﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class MothLight : PlayerInteractable, IPointerClickHandler
{
	public static List<MothLight> s_allMothLights = new List<MothLight>();
    public ParticleSystem zapp_particles;

    public bool m_isClickable = true;
	public bool m_isLit = false;
	public float m_lightStrength = 1f;
	public float m_lightDistMod = 1f;
	
	public Light m_light;
    public AudioClip zapp_clip;
    public AudioClip on_clip;
    public AudioClip off_clip;
    private AudioSource source;

    void Start()
	{
		if (s_allMothLights == null)
			s_allMothLights = new List<MothLight>();
		
		s_allMothLights.Add(this);
        source = GetComponent<AudioSource>();

        SetLit(m_isLit);
	}

	void OnDestroy()
	{
		s_allMothLights.Remove(this);
	}

	protected override void OnPlayerHit(mothmovement _moth)
	{
        if (m_isLit)
        {
            ParticleSystem ps = Instantiate(zapp_particles, _moth.gameObject.transform.position, Quaternion.identity) as ParticleSystem;
            source.clip = zapp_clip;
            source.Play();
            Destroy(_moth.gameObject);
        }
	}

	void SetLit(bool _setLit)
	{
		m_isLit = _setLit;
		m_light.enabled = m_isLit;
        if (_setLit)
        {
            source.clip = on_clip;
            source.Play();
        }
        else
        {
            source.clip = off_clip;
            source.Play();
        }
    }

	public void OnPointerClick(PointerEventData eventData)
	{
		if (!m_isClickable)
			return;
		SetLit(!m_isLit);
		Debug.LogFormat("Clicked: {0}", gameObject.name);
	}
}
