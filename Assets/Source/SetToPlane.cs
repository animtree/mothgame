﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.UIElements.StyleEnums;

public class SetToPlane : MonoBehaviour
{
	public enum Plane
	{
		BackGround = -2,
		GamePlay = 0,
		Obstructions = 1,
		ForeGround = 2,
	}

	private readonly static Dictionary<Plane, float> s_planeZOffsets = new Dictionary<Plane, float>()
	{
		{Plane.BackGround, 12f},
		{Plane.GamePlay, -1f},
		{Plane.Obstructions, -1f},
		{Plane.ForeGround, -2f},
	};

	public Plane m_plane = Plane.GamePlay;
	
	void AlignToPLane()
	{
		var pos = transform.position;
		pos.z = s_planeZOffsets[m_plane];
		transform.position = pos;
	}
	
	// Use this for initialization
	void Start () {
		AlignToPLane();
	}
	
	// Update is called once per frame
	void Update () {
		AlignToPLane();
	}
}
