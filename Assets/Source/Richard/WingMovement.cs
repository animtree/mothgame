﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WingMovement : MonoBehaviour
{
	public float flap_speed = 3.0f;
	public float flap_amount = 30.0f;
	public float flap_speed_min = 50.0f;
	public float flap_speed_max = 100.0f;
	public float flap_amount_min = 30.0f;
	public float flap_amount_max = 50.0f;
	public Vector3 flap_axis = Vector3.forward;
	private Quaternion leftwing_initial_ang;
	private Quaternion rightwing_initial_ang;
	public GameObject leftwing;
	public GameObject rightwing;

	void Start()
	{
		leftwing_initial_ang = transform.localRotation;
		rightwing_initial_ang = transform.localRotation;
		flap_speed = Random.Range (flap_speed_min, flap_speed_max);
		flap_amount = Random.Range (flap_amount_min, flap_amount_max);
	}

	void Update()
	{
		float flap_angle = Mathf.Sin (Time.time * flap_speed) * flap_amount;
		leftwing.transform.localRotation = leftwing_initial_ang * Quaternion.Euler(flap_angle * flap_axis);
		rightwing.transform.localRotation = rightwing_initial_ang * Quaternion.Euler(-flap_angle * flap_axis);

	}
	
}
