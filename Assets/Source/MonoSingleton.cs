﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public abstract class MonoSingleton<T> : MonoBehaviour where T : MonoSingleton<T>
{
	private static T m_Instance = null;
	public static T Me
	{
		get
		{
			return m_Instance;
		}
	}

	// If no other monobehaviour request the instance in an awake function
	// executing before this one, no need to search the object.
	private void Awake()
	{
		if (m_Instance == null) {
			m_Instance = this as T;
		} else if (m_Instance != this) {
			Destroy(this.gameObject);
		}
	}
}