﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
public class GizmoSphere : MonoBehaviour
{
	public Color m_colour = Color.yellow;
	public float m_radius = 0.5f;

	void OnDrawGizmos()
	{
		Gizmos.color = m_colour;
		Gizmos.DrawSphere(transform.position, m_radius);
	}
}
